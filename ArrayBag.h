/**
 * @file   ArrayBag.h
 * @author Jim Daehn
 * @brief  Header file for an array-based implementation of the ADT bag.
 */

#ifndef ARRAY_BAG_H_
#define ARRAY_BAG_H_

#include "BagInterface.h"

/**
 * ArrayBag is an array-based implementation of the ADT bag.
 */
template<typename ItemType>
class ArrayBag: public BagInterface<ItemType> {
public:
	ArrayBag();
	virtual int getCurrentSize() const override;
	virtual bool isEmpty() const override;
	virtual bool add(const ItemType& newEntry) override;
	virtual bool remove(const ItemType& anEntry) override;
	virtual void clear() override;
	virtual bool contains(const ItemType& anEntry) const override;
	virtual int getFrequencyOf(const ItemType& anEntry) const override;
	virtual std::vector<ItemType> toVector() const override;
	ArrayBag<ItemType> getUnionWithBag(ArrayBag<ItemType> aBag);
	ArrayBag<ItemType> getIntersectionWithBag(ArrayBag<ItemType> aBag);
	ArrayBag<ItemType> getDifferenceWithBag(ArrayBag<ItemType> aBag);
	virtual ~ArrayBag() {}
private:
	static const int DEFAULT_CAPACITY = 6;
	static const int INDEX_NOT_FOUND = -1;
	ItemType items[DEFAULT_CAPACITY];
	int itemCount;
	int maxItems;

	/**
	 * Returns either the index of the element in the array items that
	 * contains the given target or INDEX_NOT_FOUND, if the array does
	 * not contain the target.
	 */
	int getIndexOf(const ItemType& target, int searchIndex) const;
	int countFrequency(const ItemType& target, int searchIndex) const;
};

#include "ArrayBag.cpp"

#endif /* ARRAY_BAG_H_ */
