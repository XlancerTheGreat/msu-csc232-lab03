/**
 * @file   ArrayBagTest.h
 * @author Jim Daehn
 * @brief  TODO: Give brief description of this class.
 */

#ifndef ARRAYBAGTEST_H_
#define ARRAYBAGTEST_H_

#include <string>
#include <cppunit/extensions/HelperMacros.h>
#include "ArrayBag.h"

class ArrayBagTest : public CPPUNIT_NS::TestFixture {
	CPPUNIT_TEST_SUITE(ArrayBagTest);

		CPPUNIT_TEST(testUnionHasExpectedFrequencyOfA);
		CPPUNIT_TEST(testUnionHasExpectedFrequencyOfB);
		CPPUNIT_TEST(testUnionHasExpectedFrequencyOfC);
		CPPUNIT_TEST(testUnionHasExpectedFrequencyOfD);
		CPPUNIT_TEST(testUnionHasExpectedFrequencyOfE);

		CPPUNIT_TEST(testIntersectionHasExpectedFrequencyOfA);
		CPPUNIT_TEST(testIntersectionHasExpectedFrequencyOfB);
		CPPUNIT_TEST(testIntersectionHasExpectedFrequencyOfC);
		CPPUNIT_TEST(testIntersectionHasExpectedFrequencyOfD);
		CPPUNIT_TEST(testIntersectionHasExpectedFrequencyOfE);

		CPPUNIT_TEST(testDifferenceHasExpectedFrequencyOfA);
		CPPUNIT_TEST(testDifferenceHasExpectedFrequencyOfB);
		CPPUNIT_TEST(testDifferenceHasExpectedFrequencyOfC);
		CPPUNIT_TEST(testDifferenceHasExpectedFrequencyOfD);
		CPPUNIT_TEST(testDifferenceHasExpectedFrequencyOfE);

	CPPUNIT_TEST_SUITE_END();

public:
    ArrayBagTest();
    virtual ~ArrayBagTest();
    void setUp();
    void tearDown();

private:
    const std::string ASSERTION_MESSAGE = "Actual should equal expected.";
    void testUnionHasExpectedFrequencyOfA();
    void testUnionHasExpectedFrequencyOfB();
    void testUnionHasExpectedFrequencyOfC();
    void testUnionHasExpectedFrequencyOfD();
    void testUnionHasExpectedFrequencyOfE();

    void testIntersectionHasExpectedFrequencyOfA();
    void testIntersectionHasExpectedFrequencyOfB();
    void testIntersectionHasExpectedFrequencyOfC();
    void testIntersectionHasExpectedFrequencyOfD();
    void testIntersectionHasExpectedFrequencyOfE();

    void testDifferenceHasExpectedFrequencyOfA();
    void testDifferenceHasExpectedFrequencyOfB();
    void testDifferenceHasExpectedFrequencyOfC();
    void testDifferenceHasExpectedFrequencyOfD();
    void testDifferenceHasExpectedFrequencyOfE();

    ArrayBag<std::string> bag;
    ArrayBag<std::string> anotherBag;
    ArrayBag<std::string> expectedBag;
    ArrayBag<std::string> actualBag;
};

#endif /* ARRAYBAGTEST_H_ */
