/**
 * @file   ArrayBag.cpp
 * @authors Jim Daehn, Joseph Sneddon, Robert Dascanio
 * @brief  Implementation of ArrayBag.
 */

#include "ArrayBag.h"
using namespace std;

template<typename ItemType>
ArrayBag<ItemType>::ArrayBag() :
		itemCount(0), maxItems(DEFAULT_CAPACITY) {
	// Intentionally empty body
}

template<typename ItemType>
bool ArrayBag<ItemType>::add(const ItemType& newEntry) {
	bool hasRoomToAdd = (itemCount < maxItems);
	if (hasRoomToAdd) {
		items[itemCount] = newEntry;
		++itemCount;
	}

	return hasRoomToAdd;
}

template<class ItemType>
std::vector<ItemType> ArrayBag<ItemType>::toVector() const {
	std::vector<ItemType> bagContents;
	for (int i = 0; i < itemCount; ++i) {
		bagContents.push_back(items[i]);
	}
	return bagContents;
}

template<class ItemType>
int ArrayBag<ItemType>::getCurrentSize() const {
	return itemCount;
}

template<class ItemType>
bool ArrayBag<ItemType>::isEmpty() const {
	return itemCount == 0;
}

template<class ItemType>
bool ArrayBag<ItemType>::remove(const ItemType& anEntry) {
	int locatedIndex{getIndexOf(anEntry, 0)};
	bool canRemoveItem{!isEmpty() && (locatedIndex > -1)};
	if (canRemoveItem) {
		--itemCount;
		items[locatedIndex] = items[itemCount];
	}
	return canRemoveItem;
}

template<class ItemType>
void ArrayBag<ItemType>::clear() {
	itemCount = 0;
}

template<class ItemType>
bool ArrayBag<ItemType>::contains(const ItemType& anEntry) const {
	bool found{false};
	int curIndex{0};
	while (!found && (curIndex < itemCount)) {
		found = (anEntry == items[curIndex]);
		if (!found) {
			++curIndex;
		}
	}
	return found;
}

template<class ItemType>
int ArrayBag<ItemType>::getFrequencyOf(const ItemType& anEntry) const {
	int frequency{0};
	int curIndex{0};

	while (curIndex < itemCount) {
		if (items[curIndex] == anEntry) {
			++frequency;
		}
		++curIndex;
	}
	return frequency;
}

template<class ItemType>
int ArrayBag<ItemType>::getIndexOf(const ItemType& target, int searchIndex) const {
	int result{INDEX_NOT_FOUND};
	if (searchIndex < itemCount) {
		if (items[searchIndex] == target) {
			result = searchIndex;
		} else {
			result = getIndexOf(target, searchIndex + 1);
		}
	}
	return result;
}

template<class ItemType>
int ArrayBag<ItemType>::countFrequency(const ItemType& target, int searchIndex) const {
	int frequency{0};
	if (searchIndex < itemCount) {
		if (items[searchIndex] == target) {
			frequency = 1 + countFrequency(target, searchIndex + 1);
		} else {
			frequency = countFrequency(target, searchIndex + 1);
		}
	}
	return frequency;
}

template<class ItemType>
ArrayBag<ItemType> ArrayBag<ItemType>::getUnionWithBag(ArrayBag<ItemType> aBag) {
	ArrayBag<ItemType> bag; // ArrayBag<ItemType> that is returned

	// Stores the itemCount of aBag locally to avoid making repeated method calls
	int itemCountaBag = aBag.getCurrentSize();

	// toVector returns the values stored in aBag's private data member items[]
	vector<ItemType> aBagVector = aBag.toVector();

	// Evaluates and stores the itemCount of the largest ArrayBag
	int sizeOfLargestArray = ( itemCount < itemCountaBag ? itemCountaBag : itemCount);

	/* This for loop generates an exact match for the expected outcome
	 [a,b,b,b,c,d,e] without needing to sort it.*/
	for (int i = 0; i < sizeOfLargestArray; ++i) {
		if (i < itemCount) {
			bag.add(items[i]);
		} // End if
		if ( i < itemCountaBag) {
			bag.add(aBagVector[i]);
		} // End if
	} // End for
	return bag;
} // End getUnionWithBag

template<class ItemType>
ArrayBag<ItemType> ArrayBag<ItemType>::getIntersectionWithBag(ArrayBag<ItemType> aBag) {
	ArrayBag<ItemType> bag; // ArrayBag<ItemType> that is returned
	bool intersection{false}; // Controls test for nested if

	// Stores the itemCount of aBag locally to avoid making repeated method calls
	int itemCountaBag = aBag.getCurrentSize();

	// toVector returns the values stored in aBag's private data member items[]
	vector<ItemType> aBagVector = aBag.toVector();

	// For loop checks each element in aBagVector for a match in the ArrayBag items
	for (int i{0}; i < itemCountaBag; ++i) {
		intersection = contains(aBagVector[i]);
		if (intersection) {

			/* Prevents duplicates in aBagVector from being added to bag unless there
			there is a corresponding amount of duplicates in the ArrayBag items */
			if ( bag.getFrequencyOf(aBagVector[i]) < getFrequencyOf(aBagVector[i])) {
				bag.add(aBagVector[i]);
			} // End nested if
		} // End if
	} // End for
	return bag;
} // End getIntersectionWithBag

template<class ItemType>
ArrayBag<ItemType> ArrayBag<ItemType>::getDifferenceWithBag(ArrayBag<ItemType> aBag) {
	ArrayBag<ItemType> bag; // ArrayBag<ItemType> that is returned
	int aBagCountFreq{0};
	int itemsCountFreq{0};

	// Finds the frequency in aBag[] and items[] of each element in items[]
	for (int i{0}; i < itemCount; ++i) {
		aBagCountFreq = aBag.countFrequency(items[i], 0);
		itemsCountFreq = countFrequency(items[i], 0);

		/* Adds itemsCountFreq number of elements into bag[] if the element isn't
			in aBag[] or already in bag[] */
		if ((!aBagCountFreq) && !(bag.contains(items[i]))) {
			itemsCountFreq -= aBagCountFreq;
			for (int b{0}; b < itemsCountFreq; ++b) {
				bag.add(items[i]);
			} // End nested for

			/* Adds itemsCountFreq number of elements into bag[] if the element occurs
				more frequently in items[] than it does in aBag[] or not already in bag[] */
		} else if ((aBagCountFreq < itemsCountFreq) && !(bag.contains(items[i]))) {
			itemsCountFreq -= aBagCountFreq;
			for (int b{0}; b < itemsCountFreq; ++b) {
				bag.add(items[i]);
			} // End nested for
		} // End else if
	} // End for
	return bag;
} // End getDifferenceWithBag
